import express from 'express';

const server = express();

server.use(express.static('dist'));


server.use('/', (req, res) => {
    res.send('<h1>Hello, it is Aliko portfolio</h1>');
});

server.listen(1326, 'localhost', () => {
    console.info('Express server is listening at http://localhost:1326');
});